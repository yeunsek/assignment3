/*
 * Author: Eun Suk Lee
 * Email:el24336@email.vccs.edu
 * Date: 21 March, 2015
 * This program will validate your account type, Username and Password.
 */
import javax.swing.JOptionPane;

public class Assignment3 
{
	public static void main(String[] args) 
	{	
		String[] choices = { "Admin", "Student", "Staff"," "};//These are choices for user
		String input = (String) JOptionPane.showInputDialog(null, "Choose account type...","Account Type",JOptionPane.QUESTION_MESSAGE, null,choices,choices[3]); 
		String ID,Password;//declare String type of ID and Password
		int i=0;
		
		switch (input)
		{ 
		case "Admin": //If user choose Admin as an account type
			for (int c = 0; c<4 ; c++)//This will gives 3 trials for user
			{
				ID = JOptionPane.showInputDialog(null,"Enter your Username: ");
				if (ID.equalsIgnoreCase("smile"))//If user type proper username, smile.
				{	
					Password = JOptionPane.showInputDialog(null,"Enter your Password: ");
					while (!Password.equals("1234") && i<2)//If password is not  correct, then it gives 3 trials
					{
						Password = JOptionPane.showInputDialog(null,"Enter your Password: ");
						i++;
					}
					if (Password.equals("1234"))//If user put correct correct username and password
					{
						JOptionPane.showMessageDialog(null,"Welcome "+input + " " +ID+ "! You can Update and read file.");
						return;
					}
					break;
				}
				if (ID.equalsIgnoreCase("happy"))//If user put happy which is correct account type for Student
				{
					input = (String) JOptionPane.showInputDialog(null, "Choose account type...","Account Type",JOptionPane.QUESTION_MESSAGE, null,choices,choices[3]);
					while (!input.equals("Student"))
					{
						input = (String) JOptionPane.showInputDialog(null, "Choose account type...","Account Type",JOptionPane.QUESTION_MESSAGE, null,choices,choices[3]);
					}
					JOptionPane.showMessageDialog(null,"Run program again with correct account type!");
					return;
				}
				if (ID.equalsIgnoreCase("fun"))//If user put fun which is correct account type for Staff
				{
					input = (String) JOptionPane.showInputDialog(null, "Choose account type...","Account Type",JOptionPane.QUESTION_MESSAGE, null,choices,choices[3]);
					while (!input.equals("Staff"))
					{
						input = (String) JOptionPane.showInputDialog(null, "Choose account type...","Account Type",JOptionPane.QUESTION_MESSAGE, null,choices,choices[3]);
					}
					JOptionPane.showMessageDialog(null,"Run program again with correct account type");
					return;
				}
				else//It will show Invalid username and use trial
				{
					c=c+1;
					JOptionPane.showMessageDialog(null,"Invalid Username!");
				}
			}
			JOptionPane.showMessageDialog(null,"Contact Administrator!");//It will lock the account
			break;//Exit Case "Admin"
		case "Student": //If user puts Student as an account type
			for (int c = 0; c<4 ; c++)//This will gives 3 trials to user
			{
				ID = JOptionPane.showInputDialog(null,"Enter your Username: ");//prompt user to put username
				if (ID.equalsIgnoreCase("happy"))//If user type proper username, happy.
				{	
					Password = JOptionPane.showInputDialog(null,"Enter your Password: ");
					while (!Password.equals("1234") && i<2)//If password is not correct(3 trials)
					{
						Password = JOptionPane.showInputDialog(null,"Enter your Password: ");
						i++;
					}
					if (Password.equals("1234"))//if password is correct then it will show welcome message
					{
						JOptionPane.showMessageDialog(null,"Welcome "+input + " " +ID+ "! You can Update and read file.");
						return;
					}
					break;
				}
				if (ID.equalsIgnoreCase("smile"))//if user puts smile which is an account type of Admin
				{
					input = (String) JOptionPane.showInputDialog(null, "Choose account type...","Account Type",JOptionPane.QUESTION_MESSAGE, null,choices,choices[3]);
					while (!input.equals("Admin"))//choose an account type again until user chooses correct account type for smile
					{
						input = (String) JOptionPane.showInputDialog(null, "Choose account type...","Account Type",JOptionPane.QUESTION_MESSAGE, null,choices,choices[3]);
					}
					JOptionPane.showMessageDialog(null,"Run program again with correct account type!");
					return;
				}
				if (ID.equalsIgnoreCase("fun"))//if user puts smile which is an account type of Admin
				{
					input = (String) JOptionPane.showInputDialog(null, "Choose account type...","Account Type",JOptionPane.QUESTION_MESSAGE, null,choices,choices[3]);
					while (!input.equals("Staff"))//choose an account type again until user chooses correct account type for fun
					{
						input = (String) JOptionPane.showInputDialog(null, "Choose account type...","Account Type",JOptionPane.QUESTION_MESSAGE, null,choices,choices[3]);
					}
					JOptionPane.showMessageDialog(null,"Run program again correct account type!");
					return;
				}
				else//if username is not correct
				{
					c=c+1;
					JOptionPane.showMessageDialog(null,"Invalid Username!");
				}
			}
			JOptionPane.showMessageDialog(null,"Contact Administrator!");
			break;//Exit Case "Student"
		case "Staff": //run case if account type is staff
			for (int c = 0; c<4 ; c++)//it gives 3trials to user
			{
				ID = JOptionPane.showInputDialog(null,"Enter your Username: ");
				if (ID.equalsIgnoreCase("fun"))//if username is correct
				{	
					Password = JOptionPane.showInputDialog(null,"Enter your Password: ");
					while (!Password.equals("1234") && i<2)//if user puts wrong password
					{
						Password = JOptionPane.showInputDialog(null,"Enter your Password: ");
						i++;
					}
					if (Password.equals("1234"))//if user puts correct password then it will show welcome message
					{
						JOptionPane.showMessageDialog(null,"Welcome "+input + " " +ID+ "! You can update,read,add,delete files.");
						return;
					}
					break;
				}
				if (ID.equalsIgnoreCase("happy"))//if user puts happy which is an account type of Student
				{
					input = (String) JOptionPane.showInputDialog(null, "Choose account type...","Account Type",JOptionPane.QUESTION_MESSAGE, null,choices,choices[3]);
					while (!input.equals("Student"))
					{
						input = (String) JOptionPane.showInputDialog(null, "Choose account type...","Account Type",JOptionPane.QUESTION_MESSAGE, null,choices,choices[3]);
					}
					JOptionPane.showMessageDialog(null,"Run program again with correct account type!");
					return;
				}
				if (ID.equalsIgnoreCase("smile"))//if user puts happy which is an account type of Admin
				{
					input = (String) JOptionPane.showInputDialog(null, "Choose account type...","Account Type",JOptionPane.QUESTION_MESSAGE, null,choices,choices[3]);
					while (!input.equals("Admin"))
					{
						input = (String) JOptionPane.showInputDialog(null, "Choose account type...","Account Type",JOptionPane.QUESTION_MESSAGE, null,choices,choices[3]);
					}
					JOptionPane.showMessageDialog(null,"Run program again with correct account type!");
					return;
				}
				else
				{
					c=c+1;
					JOptionPane.showMessageDialog(null,"Invalid Username!");
				}
			}
			JOptionPane.showMessageDialog(null,"Contact Administrator!");
			break;//Exit Case "Staff"
			}
	}
}